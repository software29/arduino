#include "Arduino.h"

// TODO: Allow nStep to be "small".
//
// TODO: Use an array and math; get rid of the
// clunky, but easy-to-read switch statement
// below.
//
// TODO: Create a class with instance variables
//
// TODO: Control multiple motors simultaneously
//
int motor_pin_1 = 8;
int motor_pin_2 = 9;
int motor_pin_3 = 10;
int motor_pin_4 = 11;

int thisStep = 0;
int stepDir  = 1;

void setup() {
  pinMode(motor_pin_1, OUTPUT);
  pinMode(motor_pin_2, OUTPUT);
  pinMode(motor_pin_3, OUTPUT);
  pinMode(motor_pin_4, OUTPUT);
}

void loop() {
  demo();
}

// This is the demo, for a short video
// See: https://www.reddit.com/r/arduino/comments/a4csj0/stepper_motor_smooth_acceleration_vs_sudden_jerky/
//
void demo() {
  moveMotor(600);
  delay(100);
  moveMotor(-600);
  delay(3000);
  moveMotor1(600);
  delay(200);
  moveMotor1(-600);
  delay(350000);
}


// TODO: precalculate, store values in an array.
//
long bar(long n) {
  return long(125000.0 * (sqrt(n) - sqrt(n-1)));
}

// This is "demo" code.  Known bug: if nSteps is less than
// the number of speed-up and slow-down steps, your motor
// WILL end up in the WRONG place.
//
void moveMotor(int nSteps) {
 if (nSteps < 0) {
   stepDir = -1;
   nSteps = -nSteps;
 } else {
   stepDir = 1;
 }
  
  unsigned long minDelay = 4123; // microseconds
  long n = 1;
  unsigned long t = bar(n);
  while (t >= minDelay) {
    step();
    microDelay(t);
    n++;
    t = bar(n);
  }

  nSteps -= 2*n;

  while (nSteps > 0) {
    step();
    microDelay(t);
    nSteps--;
  }

  while (n > 1) {
    n--;
    t = bar(n);
    step();
    microDelay(t);    
  }
}


// The sudden start/stop version, for demo purposes.
//
void moveMotor1(int nSteps) {
 if (nSteps < 0) {
   stepDir = -1;
   nSteps = -nSteps;
 } else {
   stepDir = 1;
 }
  
  unsigned long minDelay = 4123; // microseconds
  for (int s = 0; s < nSteps; s++) {
    step();
    microDelay(minDelay);
  }
}


void microDelay(unsigned long microSeconds) {
  unsigned long term = micros() + microSeconds;

  while (micros() < term);
}

void step() {
  thisStep += stepDir;
  switch (thisStep % 4) {
      case 0:  // 1010
        digitalWrite(motor_pin_1, HIGH);
        digitalWrite(motor_pin_2, LOW);
        digitalWrite(motor_pin_3, HIGH);
        digitalWrite(motor_pin_4, LOW);
      break;
      case 1:  // 0110
        digitalWrite(motor_pin_1, LOW);
        digitalWrite(motor_pin_2, HIGH);
        digitalWrite(motor_pin_3, HIGH);
        digitalWrite(motor_pin_4, LOW);
      break;
      case 2:  //0101
        digitalWrite(motor_pin_1, LOW);
        digitalWrite(motor_pin_2, HIGH);
        digitalWrite(motor_pin_3, LOW);
        digitalWrite(motor_pin_4, HIGH);
      break;
      case 3:  //1001
        digitalWrite(motor_pin_1, HIGH);
        digitalWrite(motor_pin_2, LOW);
        digitalWrite(motor_pin_3, LOW);
        digitalWrite(motor_pin_4, HIGH);
      break;
    }
}
